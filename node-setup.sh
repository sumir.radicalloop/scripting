#!/bin/bash

# colors
color_off='\033[0m'
red='\033[1;91m'                 
yellow='\033[1;93m'
blue='\033[1;94m'

message(){
    echo -e "\n"
    echo -e "$yellow $@ $color_off"
    echo -e "\n"
    sleep "2"
}

false(){
    echo -e "$red $@ $color_off"
}

true(){
    echo -e "$blue $@ $color_off"
}

message "Get version of NODE from User"
while [ -z "$NODE" ]
do
    read -p "Enter the Node Version ???" NODE
done

message "Installing NODE"
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.1/install.sh | bash
source ~/.bashrc
nvm install $NODE
 
if node -v | grep $NODE >> \dev\null
then
        true "NODE successfully installed"
else
        false "NODE not successfully installed"
fi