#!/bin/bash
port=(80 443 3306 8080)
for i in "${port[@]}"
do
        if sudo netstat -tulpn | grep :$i 
        then
                sudo kill -9 $(sudo lsof -t -i:$i)
        else
                echo "port available $i"
        fi
done

#port=(80 443 3306 8080)
#i=0
#while [ $i -lt "${#port[@]}" ]
#do 
#        if sudo netstat -tulpn | grep :${port[$i]} 
#        then
#                sudo kill -9 $(sudo lsof -t -i:${port[$i]})
#       else
#               echo "port available ${port[$i]}"
#       fi
#       i=`expr $i + 1`
#done