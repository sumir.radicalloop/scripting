#!/bin/bash

# colors
color_off='\033[0m'
red='\033[1;91m'          
green='\033[1;92m'        
yellow='\033[1;93m'
blue='\033[1;94m'

success() {
    sleep "2"
    echo -e "$green $@ $color_off"
    sleep "2"
}

message(){
    echo -e "\n"
    echo -e "$yellow $@ $color_off"
    echo -e "\n"
    sleep "2"
}

false(){
    echo -e "$red $@ $color_off"
}

true(){
    echo -e "$blue $@ $color_off"
}

#installing the nginx
message "Installing Nginx"
sudo apt-get install -y nginx
sudo systemctl enable nginx.service
sudo chmod -R 755 /var/www/html
sudo chown -R $USER:$USER /var/www/html
sudo systemctl restart nginx.service
success "Nginx Successfully installed"

message "Nginx Service status"
if sudo systemctl status nginx.service | grep "running" >> \dev\null
then
        true "Nginx service running"
else
        false "Nginx service not running"
fi