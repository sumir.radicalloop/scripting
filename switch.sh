echo "Enter a character:"
read var
case $var in
  [a-z]) echo "You entered a lowercase character.";;
  [A-Z]) echo "You entered an uppercase character.";;
  [0-9]) echo "You entered a digit.";;
  ?) echo "You entered a special character.";;
  *) echo "You entered multiple characters.";;
esac
