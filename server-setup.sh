#!/bin/bash

# colors
color_off='\033[0m'
red='\033[1;91m'          
green='\033[1;92m'        
yellow='\033[1;93m'
blue='\033[1;94m'

success() {
    sleep "2"
    echo -e "$green $@ $color_off"
    sleep "2"
}

message(){
    echo -e "\n"
    echo "--------------------------------------------------------------------------------"
    echo -e "$yellow $@ $color_off"
    echo -e "\n"
    sleep "2"
}

false(){
    echo -e "$red $@ $color_off"
}

true(){
    echo -e "$blue $@ $color_off"
}

message "Get password for MySql Root Account from User "
while [ -z "$sql" ]
do
    read -p "Enter the Password of Mysql Root account???" sql
done

#The -z flag causes test to check whether a string is empty
message "Get version for PHP from User"
while [ -z "$r" ]
do
    read -p "Enter the version of Php :" r
done

message "Installing Php"
sudo apt-get update
sudo apt -y install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update

sudo apt-get install -y php$r-fpm
sudo apt-get install -y php$r-common php$r-mysql php$r-zip php$r-gd php$r-mbstring php$r-curl php$r-xml php$r-bcmath
success "Php Successfuly installed"

message "get the answer for installing Mysql"
while [ -z "$var" ]
do
    read -p "Do you want to install mysql yes or no:" var
done

case $var in
  yes|y|Y) 
        message "Installing MYSQL"
        sudo apt-get install -y mysql-server
        sudo systemctl enable mysql.service
        success "MYSQL Successfully installed"
        sudo mysql -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$sql';"
        ;;
  no|n|N)
        false "You have enter no for Mysql installation";;
esac


#installing the nginx
message "Installing Nginx"
sudo apt-get install -y nginx
sudo systemctl enable nginx.service
sudo chmod -R 755 /var/www/html
sudo chown -R $USER:$USER /var/www/html
sudo systemctl restart nginx.service
success "Nginx Successfully installed"

#installing the composer
message "Installing Composer"
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/local/bin --filename=composer
success "composer Successfully installed"

message "PHP Service Status"
if sudo systemctl status php$r-fpm.service | grep "running" >> \dev\null
then
        true "Php service running"
else
        false "Php service not running"
fi

message "Mysql Service status"
if sudo systemctl status mysql.service | grep "running" >> \dev\null
then
        true "Mysql service running"
else
        false "mysql service not running"
fi

message "Nginx Service status"
if sudo systemctl status nginx.service | grep "running" >> \dev\null
then
        true "Nginx service running"
else
        false "Nginx service not running"
fi

case $var in
  yes|y|Y) message "Mysql Root Account Password is :- $sql" ;;
  no|n|N) message "You have enter no for Mysql installation" ;;
esac
