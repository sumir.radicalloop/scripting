#!/bin/bash

#colors
red='\033[1;91m'
blue='\033[1;94m'
green='\033[1;92m'
color_off='\033[0m'

message(){
    	echo -e "$green $@ $color_off"
}
true(){
	echo -e "$blue $@ $color_off"
}
false(){
        echo -e "$red $@ $color_off"
}

while [ -z $1 ]
do
	set ${1:-development}
done

message "it will pull and push firstrepo-rl(development branch) and firstrepo-client(development branch)"
cd /home/rl7/firstrepo-rl
if git checkout $1 && git pull origin $1 && git pull gitlab $1 && git push origin $1 && git push gitlab $1
then
	true "successful pull and push in firstrepo-rl repo"
else
	false "not successful update firstrepo-rl"
	exit
fi

message "-----------------------------------------------------------------------------------"

message "it will pull on firstrepo-client repo"
cd /home/rl7/firstrepo-client

if git pull origin $1
then
	true "successful pull in firstrepo-client repo"
else
	false "not successful pull in firstrepo-client repo"
fi
