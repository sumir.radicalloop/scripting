#!/bin/bash
# This script monitors CPU and memory usage

while :
do 
  # Get the current usage of CPU and memory
  CPU_USAGE=$(top -n 1| awk '/Cpu/ { print $2}')
  MEMORY_USAGE=$(free -m | awk '/Mem/{print $3}')

  # Print the usage
  echo "CPU Usage: $CPU_USAGE%"
  echo "Memory Usage: $MEMORY_USAGE MB"
 
  # Sleep for 1 second
  sleep 1
done
