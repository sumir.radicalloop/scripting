#!/bin/bash

# colors
color_off='\033[0m'
red='\033[1;91m'          
green='\033[1;92m'        
yellow='\033[1;93m'
blue='\033[1;94m'

success() {
    sleep "2"
    echo -e "$green $@ $color_off"
    sleep "2"
}

message(){
    echo -e "\n"
    echo -e "$yellow $@ $color_off"
    echo -e "\n"
    sleep "2"
}

false(){
    echo -e "$red $@ $color_off"
}

true(){
    echo -e "$blue $@ $color_off"
}

message "Get password for MySql Root Account from User "
while [ -z "$sql" ]
do
    read -p "Enter the Password of Mysql Root account???" sql
done

message "Installing MYSQL"
sudo apt-get install -y mysql-server
sudo systemctl enable mysql.service
success "MYSQL Successfuly installed"
sudo mysql -u root -e "ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '$sql';"

message "Mysql Service status"
if sudo systemctl status mysql.service | grep "running" >> \dev\null
then
        true "Mysql service running"
else
        false "mysql service not running"
fi

message "Mysql Root Account Password is :- $sql" ;;