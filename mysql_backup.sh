#!/bin/bash
user="root"
password="root"
db_name="travel1"
backup_path="/home/rl7/mysql-backup"
filename=mydb_backup_`date +\%y_\%m_\%d_\%H_\%M`.sql
#mysql backup the data
mysqldump -u $user -p$password --databases $db_name | gzip > $backup_path/${filename}.gz

# delete the data  every 2 min
find $backup_path/* -mmin +2 -exec rm {} \;

#transfer the data from server
rsync -av $backup_path ubuntu@65.0.168.29:/home/ubuntu

#delete the data of server every 2min 
ssh ubuntu@65.0.168.29 "find /home/ubuntu/mysql-backup/* -mmin +2 -exec rm {} \;"
