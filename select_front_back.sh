#!/bin/bash
green='\033[1;92m'
color_off='\033[0m'
message(){
    	echo -e "$green $@ $color_off"
}

cd /var/www/html/kazan-demo 
case $1 in 
        --frontend) 
                message "updating frontend"
		cd frontend/
		source ~/.nvm/nvm.sh
		nvm use 18
		yarn
		yarn build
		;;
        --backend)
                message "updating backend"
                cd backend/
                source ~/.nvm/nvm.sh
                nvm use 18
                yarn
		;;
        *)
                message "updating frontend"
                cd frontend/
                source ~/.nvm/nvm.sh
                nvm use 18
                yarn
                yarn build
		cd ..
		message "updating backend"
                cd backend/
                yarn
                ;;
esac
