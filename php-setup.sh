#!/bin/bash

# colors
color_off='\033[0m'
red='\033[1;91m'          
green='\033[1;92m'        
yellow='\033[1;93m'
blue='\033[1;94m'

success() {
    sleep "2"
    echo -e "$green $@ $color_off"
    sleep "2"
}

message(){
    echo -e "\n"
    echo -e "$yellow $@ $color_off"
    echo -e "\n"
    sleep "2"
}

false(){
    echo -e "$red $@ $color_off"
}

true(){
    echo -e "$blue $@ $color_off"
}

message "Get version for PHP from User"
while [ -z "$r" ]
do
    read -p "Enter the version of Php :" r
done

message "Installing Php"
sudo apt-get update
sudo apt -y install software-properties-common
sudo add-apt-repository ppa:ondrej/php
sudo apt-get update

sudo apt-get install -y php$r-fpm
sudo apt-get install -y php$r-common php$r-mysql php$r-zip php$r-gd php$r-mbstring php$r-curl php$r-xml php$r-bcmath
success "Php Successfuly installed"

message "PHP Service Status"
if sudo systemctl status php$r-fpm.service | grep "running" >> \dev\null
then
        true "Php service running"
else
        false "Php service not running"
fi