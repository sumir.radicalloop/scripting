#!/bin/bash
db_name="travel"
backup_path="/home/rl7"
foldername="mongo_backup"
filename="mydb_backup_`date +\%y_\%m_\%d_\%H_\%M`"

server_user_name="ubuntu"
server_ip_name="43.205.115.39"
server_path="/home/ubuntu"

#mongodb data backup
mongodump --db $db_name --gzip --archive=$backup_path/$foldername/$filename.gz

#delete backup data every 2 min
find $backup_path/$foldername/* -mmin +2 -exec rm {} \;

#transfer the data  to server
rsync -av $backup_path/$foldername $server_user_name@$server_ip_name:$server_path

#delete the backup of mongodb_data on server every 2 minute
ssh $server_user_name@$server_ip_name "find $server_path/$foldername/* -mmin +2 -exec rm {} \;"
